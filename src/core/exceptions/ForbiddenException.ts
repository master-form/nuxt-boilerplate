import {HttpException} from '@/exceptions/HttpException';

export class ForbiddenException extends HttpException {
	constructor(readonly message: string, readonly code?: number) {
		super(message, 403, code);
	}
}
