import {Exception} from '@/exceptions/Exception';

export abstract class HttpException extends Exception {
	protected constructor(readonly message: string, readonly status: number, readonly code?: number) {
		super();
	}

	get name(): string {
		return this.constructor.name;
	}
}
