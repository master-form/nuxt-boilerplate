/* eslint-disable @typescript-eslint/explicit-module-boundary-types */

import {BadRequestException} from './BadRequestException';
import {UnauthorizedException} from './UnauthorizedException';
import {ForbiddenException} from './ForbiddenException';
import {NotFoundException} from './NotFoundException';
import {InternalServerErrorException} from './InternalServerErrorException';
import {UndefinedException} from './UndefinedException';

export * from './Exception';
export * from './BadRequestException';
export * from './ForbiddenException';
export * from './InternalServerErrorException';
export * from './NotFoundException';
export * from './UnauthorizedException';
export * from './UndefinedException';

export const httpExceptionFactory = (message: string, status: number, code?: number) => {
	switch (status) {
		case 400: {
			return new BadRequestException(message, code);
		}
		case 401: {
			return new UnauthorizedException(message, code);
		}
		case 403: {
			return new ForbiddenException(message, code);
		}
		case 404: {
			return new NotFoundException(message, code);
		}
		case 500: {
			return new InternalServerErrorException(message, code);
		}
		default:
			return new UndefinedException(message, code);
	}
};
