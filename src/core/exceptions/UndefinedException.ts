import {HttpException} from '@/exceptions/HttpException';

export class UndefinedException extends HttpException {
	constructor(readonly message: string, readonly code?: number) {
		super(message, 0, code);
	}
}
