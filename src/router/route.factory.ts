import {RouteConfig} from '@nuxt/types/config/router';
import {LOCALES} from '@/core/types/locales';
import {routes} from '@/router/routes';

export const routeFactory = (): RouteConfig[] => {
	return Object.values(LOCALES).reduce((newRoutes: RouteConfig[], locale) => {
		return [
			...newRoutes,
			...routes.map((route) => {
				return {
					...route,
					path: locale === LOCALES.UK ? route.path : `/${locale}/${route.path}`.toLocaleLowerCase(),
					name: `${route.name}___${locale}`.toLocaleLowerCase()
				};
			})
		];
	}, []);
};
