import {RouteConfig} from '@nuxt/types/config/router';
import {ROUTE} from '@/core/types';
import IndexPage from '@/pages/index.vue';

export const routes: RouteConfig[] = [
	{
		name: ROUTE.WELCOME,
		path: '',
		component: IndexPage
	}

	// {
	//   path: '/es/second',
	//   component: SecondPage,
	//   name: 'SecondPage___es',
	// },
];
