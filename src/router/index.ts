import Vue from 'vue';
import Router from 'vue-router';
import {routeFactory} from '@/router/route.factory';

Vue.use(Router);

export function createRouter(): Router {
	return new Router({
		mode: 'history',
		routes: routeFactory()
	});
}
