import {Module, Mutation, VuexModule} from 'vuex-module-decorators';

@Module({
	name: 'AppStore',
	stateFactory: true,
	namespaced: true
})
export default class AppStore extends VuexModule {
	flag = false;

	@Mutation
	toggle(): void {
		this.flag = !this.flag;
	}
}
