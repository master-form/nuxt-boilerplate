/* eslint-disable */
import { Store } from 'vuex';
import { getModule } from 'vuex-module-decorators';
import { config } from 'vuex-module-decorators'
import AppStore from '@/store/AppStore';
// Set rawError to true by default on all @Action decorators
config.rawError = true

let appStore: AppStore;

function initialiseStores(store: Store<any>): void {
  appStore = getModule(AppStore, store);
}

export { initialiseStores, appStore };
