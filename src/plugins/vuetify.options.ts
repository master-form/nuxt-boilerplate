import {Context} from '@nuxt/types';

export default function ({app}: Context) {
	return {
		lang: {
			t: (key: string, ...params: any[]) => app.i18n.t(key, params)
		},
		breakpoint: {},
		rtl: true,
		theme: {}
	};
}
