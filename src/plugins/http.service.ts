/* eslint-disable no-useless-constructor,no-param-reassign */
import {Context} from '@nuxt/types';
import {NuxtAxiosInstance} from '@nuxtjs/axios';
import {httpExceptionFactory} from '@/core/exceptions';
import {CollectionFetchResponse} from '@zidadindimon/vue-mc';

const axiosResponseErrorInterceptor = (error: any) => {
	// transform response error here
	const {status} = error.response || {status: null};
	const {code, message} = error.toJSON();
	const {error: exception} = error.response?.data || {error: {message}};
	// Any status codes that falls outside the range of 2xx cause this function to trigger
	// Do something with response error
	throw httpExceptionFactory(exception.message, status, code);
};

export class HttpService {
	private static instance: HttpService;

	constructor(private readonly httpClient: NuxtAxiosInstance) {}

	static init(httpClient: NuxtAxiosInstance) {
		this.instance = !this.instance ? new HttpService(httpClient) : this.instance;
		return this.instance;
	}

	static getInstance(): HttpService {
		return this.instance;
	}

	async fetch<Return, Data = any>(path: string, params?: Data): Promise<Return> {
		const {data} = await this.httpClient.get<Return>(path, {params});
		return data;
	}

	async post<Return = any, Data = any>(path: string, params: Data): Promise<Return> {
		const {data} = await this.httpClient.post<Return>(path, params, {withCredentials: true});
		return data;
	}

	async fetchList<Return, Data = any>(path: string, params?: Data): Promise<CollectionFetchResponse<Return>> {
		const {data} = await this.httpClient.get<Return[]>(path, {params});

		if (Array.isArray(data)) {
			return {
				content: data as Return[],
				pages: 1,
				page: 1
			};
		}
		// eslint-disable-next-line no-throw-literal
		throw 'FetchList not implement';
		//
		// // const {offset, limit, count} = data.meta || {offset: 0, limit: 0, count: 0};
		// return {
		//   content: data,
		//   pages: 1
		//   pages: Math.ceil(count / limit),
		//   page: Math.ceil(offset / limit),
		// };
	}
}

export default function ({$axios}: Context): void {
	HttpService.init($axios);
	$axios.onRequest((config) => {
		config.withCredentials = true;

		return config;
	});
	$axios.interceptors.response.use((response) => response, axiosResponseErrorInterceptor);
}
