/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import {NuxtConfig} from '@nuxt/types';

const config: NuxtConfig = {
	build: {
		postcss: {
			// Add plugin names as key and arguments as value
			// Install them before as dependencies with npm or yarn
			plugins: {
				// Disable a plugin by passing false as value
				'postcss-url': false,
				'postcss-nested': {},
				'postcss-responsive-type': {},
				'postcss-hexrgba': {}
			}
			// preset: {
			//   // Change the postcss-preset-env settings
			//   autoprefixer: {
			//     grid: true
			//   }
			// }
		},
		/*
		 ** Run ESLint on save
		 */
		extend(conf, {isDev, isClient}) {
			if (isDev && isClient) {
				conf.module?.rules.push({
					enforce: 'pre',
					test: /\.(js|vue)$/,
					loader: 'eslint-loader',
					exclude: /(node_modules)/
				});
			}
		}
	},
	srcDir: 'src/',
	buildModules: [
		// '@nuxtjs/tailwindcss',
		'@nuxt/typescript-build',
		'@nuxtjs/router',
		'@nuxtjs/vuetify'
	],
	css: [],
	env: {},
	head: {
		title: 'Nuxt Boilerplate',
		meta: [
			{charset: 'utf-8'},
			{name: 'viewport', content: 'width=device-width, initial-scale=1'},
			{hid: 'description', name: 'description', content: 'Nuxt.js project'}
		],
		link: []
	},
	loading: {color: '#0c64c1'},
	modules: [
		[
			'nuxt-i18n',
			{
				detectBrowserLanguage: {
					useCookie: true,
					cookieKey: 'i18n_redirected'
				},
				parsePages: false,
				defaultLocale: 'uk',
				langDir: 'locales/',
				lazy: true,
				locales: [
					{code: 'en', iso: 'en-US', file: 'en.json', name: 'English'},
					{code: 'uk', iso: 'uk-UA', file: 'uk.json', name: 'Українська'},
					{code: 'ru', iso: 'ru-RU', file: 'ru.json', name: 'Русский'}
				]
			}
		]
	],
	routerModule: {
		fileName: 'router/index.ts'
	},
	alias: {
		'@/*': '<srcDir>/*',
		'assets/*': '<srcDir>/assets/*' // (unless you have set a custom `dir.assets`)
	}
};

export default config;
