module.exports = {
	env: {
		browser: true,
		es2021: true,
		node: true
	},
	extends: [
		'eslint:recommended',
		'airbnb-base',
		'plugin:vue/essential',
		// 'plugin:vue/vue3-recommended',
		'plugin:nuxt/recommended',
		'plugin:@typescript-eslint/recommended',
		'plugin:prettier/recommended'
	],
	parser: 'vue-eslint-parser',
	parserOptions: {
		ecmaVersion: 12,
		parser: '@typescript-eslint/parser',
		sourceType: 'module'
	},
	plugins: ['vue', '@typescript-eslint'],
	rules: {
		'import/no-unresolved': 'off',
		'import/no-extraneous-dependencies': 'off',
		'no-empty': 'off',
		'max-len': ['error', {code: 150}],
		'vue/max-len': [
			'error',
			{
				code: 120,
				template: 150
			}
		],
		'import/prefer-default-export': 'off',
		'class-methods-use-this': 'off',
		'prefer-destructuring': [
			'error',
			{
				VariableDeclarator: {
					array: false,
					object: true
				},
				AssignmentExpression: {
					array: true,
					object: true
				}
			},
			{
				enforceForRenamedProperties: false
			}
		],
		'import/extensions': [
			'error',
			'ignorePackages',
			{
				js: 'never',
				jsx: 'never',
				ts: 'never',
				tsx: 'never'
			}
		]
	},
	settings: {
		'import/resolver': {
			'eslint-import-resolver-custom-alias': {
				alias: {
					'@': './src'
				},
				extensions: ['.ts', '.js', '.vue']
			},
			node: {
				moduleDirectory: ['node_modules', 'src']
			},
			nuxt: {
				extensions: ['.ts', '.js', '.vue']
			}
		}
	}
};
